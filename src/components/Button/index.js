import React from 'react';
import { Image, StyleSheet, TouchableOpacity } from 'react-native';
import { icons } from 'assets';
import Text from '../Text';

export default function Button(props) {
  return (
    <TouchableOpacity
      style={[
        props.style,
        styles.container,
        styles[`container${props.variant}`],
      ]}
      onPress={props.onPress}
    >
      {props.variant ? (
        <Image source={icons[props.variant]} style={{ marginRight: 8 }} />
      ) : null}
      <Text style={[styles['text'], styles[`text${props.variant}`]]}>
        {props.children}
      </Text>
    </TouchableOpacity>
  );
}

Button.defaultProps = {
  variant: '',
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
    padding: 10,
    borderRadius: 4,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containergoogle: {
    backgroundColor: 'rgba(244, 67, 54, 0.1)',
  },
  containercar: {
    backgroundColor: 'white',
    borderColor: '#212121',
    borderWidth: 1,
  },
  text: { color: 'white', fontSize: 16 },
  textgoogle: {
    color: 'rgb(244, 67, 54)',
  },
  containerfacebook: {
    backgroundColor: 'rgba(32, 150, 243, 0.1)',
  },
  textfacebook: {
    color: 'rgb(32, 150, 243)',
  },

  textcar: {
    backgroundColor: 'white',
    color: 'black',
  },
});
