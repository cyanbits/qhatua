import React from 'react';
import { TouchableOpacity, Image, StyleSheet, View } from 'react-native';
import { Text } from 'components';
import { images } from 'assets';
export default function Product(props) {
  return (
    <TouchableOpacity
      style={[props.style, styles.container]}
      onPress={props.onPress}
    >
      {props.discount ? (
        <View style={styles.discount}>
          <Text style={styles.disscountText}>-{props.discount}%</Text>
        </View>
      ) : null}

      {props.image ? (
        <Image source={{ uri: props.image }} style={styles.image} />
      ) : null}

      {props.image ? <Image source={images.categories[props.image]} /> : null}

      <Text style={styles.text}>{props.title}</Text>
      <Text style={styles.textprice}>
        {props.current}
        {props.price}
      </Text>
    </TouchableOpacity>
  );
}
Product.defaultProps = {
  image: '',
};
const styles = StyleSheet.create({
  container: {
    // backgroundColor: 'orange',

    flex: 1,
    marginBottom: 20,
    paddingHorizontal: 10,
  },
  image: {
    width: '100%',
    borderRadius: 4,
    flex: 1,
  },
  text: {
    fontSize: 14,
    marginTop: 8,
  },
  textprice: {
    marginTop: 8,
    fontSize: 20,
    fontWeight: 'bold',
  },
  discount: {
    right: 10,
    backgroundColor: 'black',
    borderTopRightRadius: 4,
    borderBottomLeftRadius: 4,
    width: 39,
    height: 24,
    justifyContent: 'center',
    alignContent: 'center',
    position: 'absolute',
  },
  disscountText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 12,
    textAlign: 'center',
  },
});
