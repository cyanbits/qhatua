import React, { useEffect, useState } from 'react';
import { StyleSheet, View, FlatList } from 'react-native';
import Product from './Product';
import { SafeAreaView } from 'react-native-safe-area-context';
import firestore from '@react-native-firebase/firestore';
import { Text } from 'components';

export default function ProductList({ route, navigation }) {
  const { categoryId } = route.params;
  const [loading, setLoading] = useState(false);
  const categoryReference = firestore()
    .collection('categories')
    .doc(categoryId);

  const productReference = firestore().collection('products');
  const [products, setProducts] = useState([]);

  async function fetchData() {
    setLoading(true);

    const querySnapshot = await productReference
      // .where('category', '==', categoryReference.id)
      .get();
    console.log('categoriess', categoryReference);

    const newProducts = [];
    querySnapshot.forEach((documentSnapshot) => {
      newProducts.push({
        id: documentSnapshot.id,
        ...documentSnapshot.data(),
      });
    });
    setProducts(newProducts);
    setLoading(false);
  }

  useEffect(() => {
    fetchData();
  }, []);

  const renderItem = ({ item, index }) => (
    <Product
      title={item.title}
      image={item.image}
      index={index}
      current={item.current}
      price={item.price}
      discount={item.discount}
      onPress={() => navigation.navigate('ProductDetail', { product: item })}
    />
  );

  return (
    <SafeAreaView edges={['bottom']} style={styles.container}>
      <FlatList
        refreshing={loading}
        data={products}
        renderItem={renderItem}
        numColumns={2}
        onRefresh={fetchData}
        contentContainerStyle={{ paddingHorizontal: 10, paddingTop: 20 }}
        keyExtractor={(item) => item.id}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    backgroundColor: 'white',
  },
});
