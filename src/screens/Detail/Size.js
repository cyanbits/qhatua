import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Text } from 'components';

export default function Size(props) {
  return (
    <View style={{ flexDirection: 'row' }}>
      {props.options.map((elem) => (
        <TouchableOpacity
          key={elem.value}
          onPress={() => props.onChange(elem.value)}
          style={{
            borderColor: '#C5C5C5',
            borderWidth: 1,
            marginRight: 10,
            height: 36,
            width: 34,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 4,
            backgroundColor: props.selected === elem.value ? 'black' : 'white',
          }}
        >
          <Text
            style={{
              color: props.selected === elem.value ? 'white' : 'black',
            }}
          >
            {elem.label}
          </Text>
        </TouchableOpacity>
      ))}
    </View>
  );
}
