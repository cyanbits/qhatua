import React from 'react';
import { Image, ScrollView } from 'react-native';

import { images } from 'assets';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function Size(props) {
  return (
    <ScrollView
      horizontal
      style={{
        flexDirection: 'row',
      }}
    >
      {props.option.map((elem) => (
        <TouchableOpacity
          key={elem.value}
          onPress={() => props.onChange(elem.value)}
          style={{
            backgroundColor: props.selected === elem.value ? 'black' : 'white',
            borderColor: '#212121',
            borderRadius: 4,
            borderWidth: 1,
            marginRight: 10,
            width: 150,
            height: 150,
            marginVertical: 15,
            marginRight: 10,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Image
            source={images.categories[elem.label]}
            style={{
              borderColor: '#212121',
              borderRadius: 4,
              width: 100,
              height: 100,
            }}
          />
        </TouchableOpacity>
      ))}
    </ScrollView>
  );
}
