import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, View, FlatList } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Size from './Size';
import Colors from './Colors';
import { images } from 'assets';
import { Text, Button } from 'components';
import { ScrollView } from 'react-native-gesture-handler';
import Carousel from 'react-native-carousel-view';
import firestore from '@react-native-firebase/firestore';

// const Detail_DATA = {
//   name: 'Polo Mickey talla completa ',
//   image: 'mickey',
//   title: 'HOLA',
//   currency: 'S/.',
//   price: 80.0,
//   discount: 50,
//   detail: 'Algodon nacional 20/1 reactivo',
//   colors: [
//     { value: 'buy', label: 'buy' },
//     { value: 'mickey', label: 'mickey' },
//     { value: 'shoes', label: 'shoes' },
//     { value: 'kiss', label: 'kiss' },
//   ],

//   sizes: [
//     { value: 'S', label: 'S' },
//     { value: 'M', label: 'M' },
//     { value: 'L', label: 'L' },
//     { value: 'XL', label: 'XL' },
//   ],

//   index: '8',
// };

export default function Detail({ route, navigation }) {
  const { product } = route.params;
  console.log('product', product);
  const [size, setSize] = useState('S');
  const options = product.sizes.map((size) => ({
    value: size,
    label: size,
  }));
  const option = product.colors.map((size) => ({
    value: size,
    label: size,
  }));

  const [color, setColor] = useState('buy');

  return (
    <SafeAreaView edges={['bottom']} style={styles.container}>
      {product.discount ? (
        <View style={styles.discount}>
          <Text style={styles.disscountText}>-{product.discount}%</Text>
        </View>
      ) : null}

      <ScrollView>
        {/* Todo: https://github.com/archriss/react-native-snap-carousel */}

        <Image source={images.categories['buy']} style={styles.images} />

        <View style={{ padding: 20 }}>
          <Text style={styles.text}>{product.name}</Text>
          {product.discount ? (
            <View style={{ flexDirection: 'row' }}>
              <Text
                style={[
                  styles.price,
                  {
                    textDecorationLine: 'line-through',
                    color: '#C5C5C5',
                    marginRight: 20,
                  },
                ]}
              >
                {product.currency}
                {product.price}
              </Text>
              <Text style={styles.price}>
                {product.currency}
                {(product.price * product.discount) / 100}
              </Text>
            </View>
          ) : (
            <Text style={styles.price}>
              {product.currency}
              {product.price}
            </Text>
          )}

          <Text style={styles.detail}>{product.detail}</Text>

          <Text style={styles.subtitle}>Tallas</Text>
          <Size
            options={options}
            selected={size}
            onChange={(value) => setSize(value)}
          />
          {console.log('options', options)}
          <Text style={styles.subtitle}>Colores</Text>

          <Colors
            option={option}
            selected={color}
            onChange={(value) => setColor(value)}
            // onPress={() => navigation.navigate('Detail', { categoryId: index.id })}
          />

          <Text style={{ marginBottom: 10 }}>
            Disponible en color : Negro, Acero, Melage, Palo Rosa y Cremoso
          </Text>
          <View style={{ flexDirection: 'row' }}>
            <Button
              variant="car"
              style={{ flex: 1, marginHorizontal: 12 }}
              onPress={() =>
                navigation.navigate('ShoppingCart', { shopping: product.id })
              }
            >
              <Text>Añadir</Text>
            </Button>
            <Button style={{ flex: 1, marginHorizontal: 12 }}>
              <Text>Comprar ahora</Text>
            </Button>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
Detail.defaultProps = {
  image: '',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  textsize: {
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 22,
    padding: 10,
  },
  images: {
    width: '100%',
    height: 375,
  },
  text: {
    fontSize: 20,
  },
  textsize: {
    color: '#212121',
    fontSize: 14,
  },
  price: {
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 22,
  },
  detail: {
    fontSize: 14,
    marginTop: 22,
  },
  subtitle: {
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 22,
    marginBottom: 15,
  },
  discount: {
    right: 10,
    backgroundColor: 'black',
    borderTopRightRadius: 4,
    borderBottomLeftRadius: 4,
    width: 70,
    height: 34,
    justifyContent: 'center',
    alignContent: 'center',
    position: 'absolute',
  },
  disscountText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 15,
    textAlign: 'center',
  },
});
