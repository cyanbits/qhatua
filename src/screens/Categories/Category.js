import React from 'react';
import { TouchableOpacity, StyleSheet, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import MaskedView from '@react-native-community/masked-view';
import { Text, Button } from 'components';
import { images } from 'assets';

const colors = [
  ['#EDF2FF', '#DCE8FF'],
  ['#E9DAD1', '#FBD6C2'],
  ['#FFE5E2', '#FFD4D2'],
  ['#F9F9F9', '#E9E9E9'],
];

export default function Category(props) {
  return (
    <TouchableOpacity
      style={[props.style, styles.container, styles[`container${props.image}`]]}
      onPress={props.onPress}
    >
      <LinearGradient
        start={{ x: 1, y: 0 }}
        end={{ x: 0, y: 0 }}
        colors={colors[props.index % colors.length]}
        style={styles.gradient}
      >
        <Text style={styles.text}>{props.title}</Text>
        {props.image ? <Image source={images.categories[props.image]} /> : null}
      </LinearGradient>
    </TouchableOpacity>
  );
}
Category.defaultProps = {
  image: '',
};

const styles = StyleSheet.create({
  container: {
    marginBottom: 10,
    height: 118,
    borderRadius: 30,
  },
  gradient: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 4,
  },
  text: {
    fontSize: 24,
    fontWeight: 'bold',
    marginLeft: 20,
  },
});
