import React, { useEffect, useState } from 'react';
import { StyleSheet, View, FlatList } from 'react-native';
import firestore from '@react-native-firebase/firestore';

import Category from './Category';

export default function Categories({ navigation }) {
  const [categories, setCategories] = useState([]);
  const [loading, setLoading] = useState(false);
  const categoriesReference = firestore().collection('categories');

  async function fetchData() {
    setLoading(true);
    const querySnapshot = await categoriesReference.get();
    const newCategories = [];
    querySnapshot.forEach((documentSnapshot) => {
      newCategories.push({
        id: documentSnapshot.id,
        ...documentSnapshot.data(),
      });
    });
    setCategories(newCategories);
    setLoading(false);
  }

  useEffect(() => {
    fetchData();
  }, []);

  const renderItem = ({ item, index }) => (
    <Category
      title={item.title}
      image={item.image}
      index={index}
      onPress={() =>
        navigation.navigate('ProductList', { categoryId: item.id, index })
      }
    />
  );
  return (
    <View style={styles.container}>
      <FlatList
        refreshing={loading}
        data={categories}
        renderItem={renderItem}
        contentContainerStyle={{
          padding: 24,
        }}
        onRefresh={fetchData}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',

    alignItems: 'stretch',
  },
});
