import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { Text } from 'components';
import { images } from 'assets';

export default function Shopping(props) {
  const renderItem = ({ item }) => <Item title={item.title} />;

  return (
    <View style={styles.container}>
      {/* <Image source={images.categories[props.image]} />
      <Text> {props.detail}</Text> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    flexDirection: 'row',
  },
  image: {
    backgroundColor: 'pink',
  },
});
