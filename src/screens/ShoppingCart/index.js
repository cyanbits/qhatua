import React from 'react';
import { View, FlatList, SafeAreaView, StyleSheet } from 'react-native';
import Shopping from './Shopping';
import { Text } from 'components';
import firestore from '@react-native-firebase/firestore';

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'First Item',
    image: 'buy',
    detail: 'Polo de algodon',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Second Item',
    image: 'buy',
    detail: 'Polo de algodon',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Third Item',
    image: 'buy',
    detail: 'Polo de algodon',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63555',
    title: 'Second Item',
    image: 'buy',
    detail: 'Polo de algodon',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d7244444',
    title: 'Third Item',
    image: 'buy',
    detail: 'Polo de algodon',
  },
];

export default function ShoppingCart({ route, navigation }) {
  const { shopingcart } = route.params;
  // const renderItem = ({ item, index }) => (
  //   <Shopping
  //     title={item.title}
  //     image={item.image}
  //     index={index}
  //     id={item.id}
  //     detail={item.detail}

  //     //   onPress={() => navigation.navigate('ProductDetail', { product: item })}
  //   />
  // );
  return (
    <SafeAreaView edges={['bottom']} style={styles.container}>
      <FlatList
        data={DATA}
        // renderItem={renderItem}
        numColumns={1}
        contentContainerStyle={{ paddingHorizontal: 10, paddingTop: 20 }}
        keyExtractor={(item) => item.id}
      />
      <View style={styles.detail}>
        <Text>Detail</Text>
        <Text>Detail</Text>
        <Text>Detail</Text>
        <Text>Detail</Text>
        <Text>Detail</Text>
        <Text>Detail</Text>
      </View>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  detail: {
    backgroundColor: 'pink',
    borderTopColor: 'black',
    borderTopWidth: 3,
    position: 'absolute',
  },
});
