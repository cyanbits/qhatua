import React from 'react';
import { Image, View, TouchableOpacity } from 'react-native';
import auth from '@react-native-firebase/auth';
import { LoginManager, AccessToken } from 'react-native-fbsdk';
import { GoogleSignin } from '@react-native-community/google-signin';

import { Text, Button } from 'components';
import LinearGradient from 'react-native-linear-gradient';
import { images } from 'assets';
import { SafeAreaView } from 'react-native-safe-area-context';

export default function Welcome({ navigation }) {
  async function handleFacebook() {
    const result = await LoginManager.logInWithPermissions([
      'public_profile',
      'email',
    ]);
    if (!result.isCancelled) {
      try {
        const { accessToken } = await AccessToken.getCurrentAccessToken();
        const facebookCredential = auth.FacebookAuthProvider.credential(
          accessToken,
        );
        const authResult = await auth().signInWithCredential(
          facebookCredential,
        );
        console.log('authResult', authResult);
        navigation.navigate('Categories');
      } catch {}
    }
  }

  async function handleGoogle() {
    GoogleSignin.configure();
    await GoogleSignin.hasPlayServices();
    const { idToken } = await GoogleSignin.signIn();
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);

    const authResult = await auth().signInWithCredential(googleCredential);
    console.log('authResult', authResult);
    navigation.navigate('Categories');
  }

  return (
    <View style={{ flex: 1, backgroundColor: 'white' }}>
      <Image
        source={images.welcome}
        style={{
          position: 'absolute',
          width: '100%',
        }}
      />
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
        locations={[0.3, 0.85]}
        colors={['#ffffff00', '#ffffffff']}
        style={{
          height: 560,
          width: '100%',
          position: 'absolute',
          left: 0,
          top: 0,
        }}
      ></LinearGradient>
      <SafeAreaView
        // edges={['bottom']}
        style={{
          flex: 1,
          alignContent: 'space-around',
        }}
      >
        <View style={{ flex: 1 }}>
          <View
            style={{
              justifyContent: 'space-between',
              alignItems: 'center',
              height: 530,
            }}
          >
            <Image
              source={images.qhatua}
              style={{ width: 180, resizeMode: 'contain' }}
            />
            <View style={{ justifyContent: 'center', margin: 10 }}>
              <Text
                style={{
                  fontSize: 26,
                  textAlign: 'center',
                  marginVertical: 20,
                }}
              >
                Porque dejar volar tu imaginación no tiene límites{' '}
              </Text>
            </View>
          </View>
        </View>

        <View
          style={{
            justifyContent: 'flex-end',
            flex: 1,
            marginVertical: 60,
            alignContent: 'stretch',
          }}
        >
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom: 10,
            }}
          >
            <TouchableOpacity
              style={{ padding: 20 }}
              onPress={() => navigation.navigate('Categories')}
            >
              <Text>Continuar como invitado</Text>
            </TouchableOpacity>
          </View>

          <View style={{ flexDirection: 'row', marginHorizontal: 12 }}>
            <Button
              onPress={handleGoogle}
              variant="google"
              style={{ flex: 1, marginHorizontal: 12 }}
            >
              Google
            </Button>
            <Button
              variant="facebook"
              onPress={handleFacebook}
              style={{ flex: 1, marginHorizontal: 12 }}
            >
              Facebook
            </Button>
          </View>
        </View>
      </SafeAreaView>
    </View>
  );
}
