export default {
  logo: require('./logo.png'),
  qhatua: require('./Qhatua.png'),
  welcome: require('./welcome.png'),

  categories: {
    buy: require('./categories/buy.png'),
    kiss: require('./categories/kiss.png'),
    shoes: require('./categories/shoes.png'),
    mickey: require('./categories/mickey.png'),
  },
};
