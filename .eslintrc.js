const path = require('path');

module.exports = {
  root: true,
  extends: [
    'plugin:prettier/recommended',
    'plugin:react/recommended',
    '@react-native-community',
    'airbnb',
    'prettier/react',
    'prettier/standard',
    'prettier',
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
  },
  settings: {
    'import/resolver': {
      'babel-module': {},
    },
  },
  plugins: ['react', 'prettier'],
  rules: {
    'react/jsx-filename-extension': [
      1,
      {
        extensions: ['.js', '.jsx'],
      },
    ],
    'no-underscore-dangle': 0,
    'import/no-extraneous-dependencies': [
      'error',
      { packageDir: path.join(__dirname) },
    ],
    'react/jsx-props-no-spreading': [
      1,
      {
        custom: 'ignore',
      },
    ],
    'react-native/no-inline-styles': [0],
    'prettier/prettier': ['error'],
    'react/default-props-match-prop-types': [
      'error',
      { allowRequiredDefaults: true },
    ],
    'react/require-default-props': [
      'error',
      { forbidDefaultForRequired: false },
    ],
    'no-console': ['warn', { allow: ['debug', 'error'] }],
  },
};
